﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using productosFiesta.codigo;

namespace productosFiesta.paginas
{
    public partial class admin : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)    
        {
            int codigo = Convert.ToInt32(TextBox1.Text);
            string descripcion = TextBox2.Text.ToString();
            int cantidad = Convert.ToInt32(TextBox3.Text);
            double costo = double.Parse(TextBox4.Text);  
            int usuario = Convert.ToInt32(TextBox5.Text);
            int tipo = Convert.ToInt32(TextBox6.Text);
            int material = Convert.ToInt32(TextBox7.Text);
            int color = Convert.ToInt32(TextBox8.Text);

            gestionProductos pro = new gestionProductos();
            

            pro.insertarProducto(codigo,descripcion,cantidad,costo,usuario,tipo,material,color);


            foreach (GridViewRow row in GridView1.Rows)
            {

                DropDownList1.Items.Add(row.Cells[0].Text);
                  

            }
            Response.Redirect("admin.aspx");   

        }



        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }


        public void actualizar()
        {
            foreach (GridViewRow row in GridView1.Rows)
            {

                DropDownList1.Items.Add(row.Cells[0].Text);


            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {

            actualizar();
            
        }

        protected void Button2_Click(object sender, EventArgs e)  //EDITAR
        {


            //int codigo = Convert.ToInt32(DropDownList1.SelectedValue);
            //string descripcion = TextBox2.Text.ToString();
            //int cantidad = Convert.ToInt32(TextBox3.Text);
            //double costo = double.Parse(TextBox4.Text);
            //int usuario = Convert.ToInt32(TextBox5.Text);
            //int tipo = Convert.ToInt32(TextBox6.Text);
            //int material = Convert.ToInt32(TextBox7.Text);
            //int color = Convert.ToInt32(TextBox8.Text);

            //gestionProductos pro = new gestionProductos(); 

            ////pro.insertarTipo(4,"adamantium"); 


            //pro.editarProducto(codigo,descripcion,cantidad,costo,usuario,tipo,material,color); 
            //actualizar();
            //Response.Redirect("admin.aspx"); 

        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            int codigo = Convert.ToInt32(DropDownList1.SelectedValue);
            gestionProductos pro = new gestionProductos();
            pro.eliminarProducto(codigo);
            actualizar();
            Response.Redirect("admin.aspx");

        }



        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {




        }




        protected void Button5_Click(object sender, EventArgs e)
        {

            if (TextBox1.Text == "" && TextBox2.Text == "" && TextBox3.Text == "" && TextBox4.Text == "" && TextBox5.Text == ""
                && TextBox6.Text == "" && TextBox7.Text == "" && TextBox8.Text == "")
            {
                foreach (GridViewRow row in GridView1.Rows)
                {

                    if (DropDownList1.SelectedValue.ToString() == row.Cells[0].Text)
                    {
                        TextBox1.Text = row.Cells[0].Text;
                        TextBox2.Text = row.Cells[1].Text;
                        TextBox3.Text = row.Cells[2].Text;
                        TextBox4.Text = row.Cells[3].Text;
                        TextBox5.Text = row.Cells[4].Text;
                        TextBox6.Text = row.Cells[5].Text;
                        TextBox7.Text = row.Cells[6].Text;
                        TextBox8.Text = row.Cells[7].Text;
                        break;
                    }

                }
            }
            else
            {
                int codigo = Convert.ToInt32(DropDownList1.SelectedValue);
                string descripcion = TextBox2.Text.ToString();
                int cantidad = Convert.ToInt32(TextBox3.Text); 
                double costo = double.Parse(TextBox4.Text);
                int usuario = Convert.ToInt32(TextBox5.Text);
                int tipo = Convert.ToInt32(TextBox6.Text);
                int material = Convert.ToInt32(TextBox7.Text);
                int color = Convert.ToInt32(TextBox8.Text);

                gestionProductos pro = new gestionProductos();

                //pro.insertarTipo(4,"adamantium"); 


                pro.editarProducto(codigo, descripcion, cantidad, costo, usuario, tipo, material, color);
               
                Response.Redirect("admin.aspx");
                actualizar();
            }


        }







    }  
}