﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient; 

namespace productosFiesta.codigo
{
    public class agregar
    {

        public void insertarProducto(int codigo, string descripcion, int cantidad, double costo, 
            int usuario, int tipo, int material, int color) 
        {
            MySqlConnection conectar = conexion.recibirconexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO productos VALUES(@a,@b,@c,@d,@e,@f,@g,@h,@i);";

                comando.Parameters.AddWithValue("@a",codigo);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", cantidad);
                comando.Parameters.AddWithValue("@d", costo);
                comando.Parameters.AddWithValue("@f", usuario);
                comando.Parameters.AddWithValue("@g", tipo);
                comando.Parameters.AddWithValue("@h", material);
                comando.Parameters.AddWithValue("@i", color);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }


            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }

        }

    }

}