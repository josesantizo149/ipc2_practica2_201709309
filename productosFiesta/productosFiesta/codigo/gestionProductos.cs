﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient; 

namespace productosFiesta.codigo
{
    public class gestionProductos
    {



        public void insertarTipo(int codigo, string nombre)
        {
            MySqlConnection conectar = conexion.RecibirConexion();

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE tipo SET nombre = @b, codigo = 9 WHERE codigo = @a ";
                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", nombre);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close(); 
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }  

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                  
            }

        }



        public void insertarProducto(int codigo, string descripcion, int cantidad, double costo,
           int usuario, int tipo, int material, int color)
        {
            MySqlConnection conectar = conexion.RecibirConexion(); 

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO producto VALUES(@a,@b,@c,@d,@e,@f,@g,@h);";

                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", cantidad);
                comando.Parameters.AddWithValue("@d", costo);
                comando.Parameters.AddWithValue("@e", usuario);
                comando.Parameters.AddWithValue("@f", tipo);
                comando.Parameters.AddWithValue("@g", material);
                comando.Parameters.AddWithValue("@h", color);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }


            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }

        }



        public void editarProducto(int codigo, string descripcion, int cantidad, double costo,
          int usuario, int tipo, int material, int color)
        {
            MySqlConnection conectar = conexion.RecibirConexion(); 

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE producto SET descripcion = @b, cantidad = @c, costo = @d, codigousuario = @e, codigotipo = @f, codigomaterial = @g," +
                    " codigocolor = @h  WHERE codigo = @a;";

                comando.Parameters.AddWithValue("@a", codigo);
                comando.Parameters.AddWithValue("@b", descripcion);
                comando.Parameters.AddWithValue("@c", cantidad);
                comando.Parameters.AddWithValue("@d", costo);
                comando.Parameters.AddWithValue("@e", usuario);
                comando.Parameters.AddWithValue("@f", tipo);
                comando.Parameters.AddWithValue("@g", material);
                comando.Parameters.AddWithValue("@h", color);

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }


            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }

        }






        public void eliminarProducto(int codigo)
        {
            MySqlConnection conectar = conexion.RecibirConexion();   

            try
            {
                conectar.Open();
                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE FROM producto WHERE codigo = @a;"; 

                comando.Parameters.AddWithValue("@a", codigo);
              

                try
                {
                    comando.ExecuteNonQuery();
                    conectar.Close();
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.ToString());
                }


            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }

        }




        

















    }
}