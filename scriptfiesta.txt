CREATE DATABASE profiesta;
USE profiesta;
CREATE TABLE tipo (codigo INT(10) NOT NULL, nombre VARCHAR(25) NOT NULL, PRIMARY KEY (codigo));
CREATE TABLE material(codigo INT(10) NOT NULL, nombre VARCHAR(25) NOT NULL, PRIMARY KEY (codigo));
CREATE TABLE color(codigo INT(10) NOT NULL, nombre VARCHAR(25) NOT NULL, PRIMARY KEY (codigo));


CREATE TABLE usuario(codigo INT(10) NOT NULL, usuario VARCHAR(25) NOT NULL, contrasena VARCHAR(25) NOT NULL,nombre VARCHAR(25) NOT NULL,apellido VARCHAR(25) NOT NULL, PRIMARY KEY (codigo));


CREATE TABLE producto (codigo int (10) NOT NULL, descripcion VARCHAR (50) NOT NULL
    ,cantidad INT (10) NOT NULL, costo DECIMAL (10,2) NOT NULL, codigousuario int (10) NOT NULL,
codigotipo int (10) NOT NULL,codigomaterial int (10) NOT NULL,codigocolor int (10) NOT NULL,
FOREIGN KEY (codigousuario) REFERENCES usuario(codigo),FOREIGN KEY (codigotipo) REFERENCES tipo(codigo),
FOREIGN KEY (codigomaterial ) REFERENCES material(codigo),FOREIGN KEY (codigocolor ) REFERENCES color(codigo),
PRIMARY KEY (codigo));